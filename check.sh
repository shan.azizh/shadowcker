#!/bin/sh

case "$XDG_SESSION_TYPE" in
  x11)
    echo "You are running X11, continue"
    ;;

  tty)
    echo "You may run i3, if not the case, make sure you are using x11/Xorg, continue"
    ;;

  *)
    echo "You need to run X11/Xorg for Shadow, You may switch from your linux login window exiting.";
    exit 1;
esac

if [ "$USER" = "root" ]; then
   echo "You need to run shadowcker with your user, not root, exiting."; 
   exit 1;
fi

