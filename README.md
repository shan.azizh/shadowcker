# Shadowcker

## Introduction

This is Shadow client in docker, this will also allow Intel HD2500-4600 iGPU to run.

## Requirements

 - Docker (make sure to add your user to **docker** group and disconnect from X11/reboot (```sudo apt install make docker.io docker-compose && sudo usermod -aG docker $USER && reboot```))
 - Support for docker compose file format 3.7+ (Docker Engine 18.06.0+)
 - Compatible GPU with DRI exposed & rights to access it (NVIDIA's GPU are not supported)
 - X11
 - **A user account** (**DO NOT** use **sudo** or **root** account)

## Build & Run

### Intel / AMD
```bash
git clone https://gitlab.com/aar642/shadowcker.git
cd shadowcker
make stable # or you can use: make beta or make alpha to change client level
make start
```

### NEW: Intel Xe / Intel / AMD (Based on Ubuntu 20.04)
```bash
git clone --branch 20.04 https://gitlab.com/aar642/shadowcker.git
cd shadowcker
make stable # or you can use: make beta or make alpha to change client level
make start
```

## Troubleshooting

### **R-0x7f** when starting Shadow

If you get this error make sure to update shadowcker and rebuild.

```bash
git pull && git checkout master https://gitlab.com/aar642/shadowcker.git
make stable # or you can use: make beta or make alpha to change client level
make start
```

### The futex facility returned an unexpected error code.Aborted

```bash
xhost +localhost && xhost +local:docker
```
### Arch / Manjaro

```bash
pacman -Syu xorg-xhost
xhost +localhost && xhost +local:docker
```
### Black Screen AMD GPU ###

Recents AMD hardware requires updated kernel (5.8+), for example:

#### Ubuntu 18.04 ####

```bash
sudo apt install linux-image-generic-hwe-18.04-edge
```
and reboot

#### Ubuntu 20.04 ####

```bash
sudo apt install linux-image-generic-hwe-20.04-edge
```
and reboot

### Start with update

Update works, but make the launcher crash. Just re-start it.

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://shdw.me/DiscordFR)
- [Discord Shadow ENG](https://shdw.me/USDiscord)
- [Discord Shadow DE](https://shdw.me/discord-de)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
[![BuyMeCoffee][buymecoffeebadge]][buymecoffee]

## Contributors
**Drixs#6784**
**Le Panzer de Rodin#9477**
**Camille**
**gtx** and countless others !

## Disclaimer

This is a community project, project is not affliated to Shadow in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Shadow](http://www.shadow.tech/).

[buymecoffee]: https://www.buymeacoffee.com/latqazuzw
[buymecoffeebadge]: https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow?style=for-the-badge
